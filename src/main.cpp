#include <Arduino.h>
#include <FastLED.h>

void buildSnowman(CRGB leds[]) {
  for(int i = 0; i <= 10; i++) {
    leds[i] = CRGB::DarkBlue;
  }
  for(int i = 11; i <= 34; i++) {
    leds[i] = CRGB::White;
  }
  leds[35] = CRGB::DarkBlue;
  for(int i = 37; i <= 40; i++) {
    leds[i] = CRGB::DarkSlateBlue;
  }
  for(int i = 41; i <= 45; i++) {
    leds[i] = CRGB::OrangeRed;
  }
  for(int i = 46; i <= 49; i++) {
    leds[i] = CRGB::DarkSlateBlue;
  }
  for(int i = 50; i <= 59; i++) {
    leds[i] = CRGB::DarkSlateBlue;
  }
}

void runChristmasAnimation(CRGB leds[], int size, int* animationRuns) {
  for(int i = 0; i <= size; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();
  for(int i = 0; i <= size; i++) {
    if (i % 2 == 0) {
      leds[i] = CRGB::Red;
    } else {
      leds[i] = CRGB::Green;
    }
    FastLED.delay(30);
  }
  for(int i = 0; i <= 10; i++) {
    if(i % 2 == 0) {
      for(int j = 0; j <= size; j++) {
        if (j % 2 == 0) {
          leds[j] = CRGB::Green;
        } else {
          leds[j] = CRGB::Red;
        }
      }
    } else {
      for(int j = 0; j <= size; j++) {
        if (j % 2 == 0) {
          leds[j] = CRGB::Red;
        } else{
          leds[j] = CRGB::Green;
        }
      }
    }
    FastLED.delay(500);
  }
}
#define LED_PIN 5
#define NUM_LEDS 60
#define BRIGHTNESS 64
#define LED_TYPE WS2811
#define COLOR_ORDER RGB
// #define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];

#define UPDATES_PER_SECOND 100

void setup() {
  Serial.begin(9600); 
  delay(3000);
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(BRIGHTNESS);
}

bool isSnowmanOn = false;
int animationRuns = 0;
void loop() {
  runChristmasAnimation(leds, NUM_LEDS, &animationRuns);
  FastLED.show();
  FastLED.delay(3000 / UPDATES_PER_SECOND);
  buildSnowman(leds);
  FastLED.delay(10000);
}